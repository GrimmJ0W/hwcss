import React from "react";
import {Box, makeStyles, Typography} from "@material-ui/core";
import IconMap from "../../photo/Icon-map.svg";
import trafficIcon from "../../photo/Icon-traffic.svg";
import publicTransportIcon from "../../photo/Transport.svg";

const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        paddingTop: theme.spacing(4)
    },
    map: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        height: theme.spacing(20),
        width: theme.spacing(39),
        backgroundColor: theme.palette.switcher.map,
        borderRadius: theme.spacing(17.2),
        margin: "auto",
        [theme.breakpoints.up('767')]:{
            margin: theme.spacing(8, 0, 3, 3)
        },
        [theme.breakpoints.up('1440')]:{
            margin: theme.spacing(23, 0, 0, 4.5),
            height: theme.spacing(34.5),
            width: theme.spacing(70),
        },

    },
    circles: {
        display: "flex",
        flexDirection: "row-reverse",
        alignItems: "center"

    },
    circle1: {
        height: theme.spacing(18),
        width: theme.spacing(18),
        borderRadius: theme.spacing(17.2),
        backgroundColor: "#c9bd5c",
        marginRight: theme.spacing(2.2),
        [theme.breakpoints.up('1440')]:{
            height: theme.spacing(31),
            width: theme.spacing(31),
        },
    },
    circle2: {
        height: theme.spacing(20),
        width: theme.spacing(20),
        borderRadius: theme.spacing(17.2),
        backgroundColor: "#c9bd5c",
        opacity: "20%",
        position: "absolute",
        marginRight: theme.spacing(-2.2),
        [theme.breakpoints.up('1440')]:{
            height: theme.spacing(34.5),
            width: theme.spacing(34.5),
            marginRight: theme.spacing(-4.5),
        },
    },
    circle3: {
        height: theme.spacing(20),
        width: theme.spacing(20),
        borderRadius: theme.spacing(17.2),
        backgroundColor: "#c9bd5c",
        opacity: "20%",
        position: "absolute",
        marginRight: theme.spacing(-4.4),
        [theme.breakpoints.up('1440')]:{
            height: theme.spacing(34.5),
            width: theme.spacing(34.5),
            marginRight: theme.spacing(-9),
        },
    },
    mapIconContainer: {
        display: "flex",
        justifyContent: "center"
    },
    mapIcon: {
        position: "absolute",
        transform: "translateY(-120px)",
        [theme.breakpoints.up('1440')]:{
            transform: "translateY(-170px)",
        },
    },
    mapText: {
        position: "absolute",
        fontSize: theme.spacing(3),
        color: theme.palette.common.white,
        transform: "translateY(-75px)",
        [theme.breakpoints.up('1440')]:{
            transform: "translateY(-110px)",
        },
    },
    realTime: {
        marginTop: theme.spacing(4),
        height: theme.spacing(20),
        width: theme.spacing(40),
        backgroundColor: theme.palette.switcher.realTime,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        margin: "auto",
        alignItems: "center",
        [theme.breakpoints.up('767')]:{
            margin: theme.spacing(0, 0, 0, 3)
        },
        [theme.breakpoints.up('1440')]:{
            margin: theme.spacing(0, 0, 0, 4),
            marginTop: theme.spacing(7.5),
            height: theme.spacing(34.5),
            width: theme.spacing(70),
        },
    },
    realTimeText: {
        fontSize: theme.spacing(3),
        color: theme.palette.common.white,
        textAlign: "center",
        margin: theme.spacing(0, 2)
    },
    traficPublicBox: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        [theme.breakpoints.up('767')]:{
            flexDirection: "row-reverse",
            justifyContent: "end",
            marginRight: theme.spacing(4)

        }
    }
}))

const MapStyleSwitcher = () => {
    const classes = useStyles();

    return (
        <Box className={classes.container}>
            <Box className={classes.map}>
                <Box className={classes.circles}>
                    <Box className={classes.circle1}/>
                    <Box className={classes.circle2}/>
                    <Box className={classes.circle3}/>
                </Box>
                <Box className={classes.mapIconContainer}>
                    <img className={classes.mapIcon} src={IconMap} alt="Map icon"/>
                    <Typography className={classes.mapText}>Map style switcher</Typography>
                </Box>
            </Box>
            <Box className={classes.traficPublicBox}>
                <Box className={classes.realTime}>
                    <img src={trafficIcon} alt="trafficIcon"/>
                    <Typography className={classes.realTimeText}>Real-time traffic date</Typography>
                </Box>
                <Box style={{backgroundColor: "#e85500"}} className={classes.realTime}>
                    <img src={publicTransportIcon} alt="public transport icon"/>
                    <Typography className={classes.realTimeText}>Schedule of public transport that is near
                        you</Typography>
                </Box>
            </Box>

        </Box>
    );
}

export default MapStyleSwitcher;
