import React from "react";
import {Box, makeStyles, Typography} from "@material-ui/core";
import iphone from "../../photo/iPhone X@3x.png";
import carpoolMadeEasy from "../../photo/carpool-mob@3x.png";
import carpoolMadeEasyTablet from "../../photo/carpool-tablet@3x.png";
import carpoolMadeEasyDesk from "../../photo/carpool-graph@3x.png";
import appleIcon from "../../photo/Apple logo.svg";
import playIcon from "../../photo/play market.svg";

const useStyles = makeStyles((theme) => ({
    container: {
        width: "100%",
        height: theme.spacing(82.5),
        backgroundColor: theme.palette.common.black,
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.up('768')]: {
            height: theme.spacing(208)
        },
        [theme.breakpoints.up('1440')]:{
            height: theme.spacing(240)
        }

    },
    imgBox: {
        display: "flex",
        width: "100%",
        justifyContent: "center",
    },
    img: {
        marginTop: theme.spacing(8),
        width: theme.spacing(23),
        objectFit: "contain",
        [theme.breakpoints.up('768')]: {
            width: theme.spacing(65),
            marginTop: theme.spacing(27)
        },
        [theme.breakpoints.up('1440')]:{
            width: theme.spacing(86),
        }
    },
    img1: {
        width: "251px",
        objectFit: "contain",
        position: "absolute",
        transform: "translateY(-350px)",
        [theme.breakpoints.up('768')]: {
            display: "none"
        }
    },
    imgBox1: {
        display: "flex",
        width: "100%",
        justifyContent: "center",
        transform: "translateX(-77px)",
        [theme.breakpoints.up('768')]: {
            display: "none"
        }

    },
    title: {
        fontFamily: "WhyteInktrap-Regular",
        fontSize: theme.spacing(4),
        color: theme.palette.common.white,
        textAlign: "center",
        transform: "translateY(-40px)",
        [theme.breakpoints.up('768')]: {
            fontSize: theme.spacing(9),
            transform: "translateY(-385px)",
        },
        [theme.breakpoints.up('1440')]: {
            transform: "translateY(-310px)",
        }
    },
    text: {
        color: theme.palette.common.white,
        textAlign: "center",
        padding: theme.spacing(0, 11),
        fontSize: theme.spacing(2),
        fontFamily: "AktivGrotesk-Regular",
        transform: "translateY(-40px)",
        [theme.breakpoints.up('768')]: {
            transform: "translateY(-385px)",
        },
        [theme.breakpoints.up('1440')]: {
            transform: "translateY(-300px)",
        }
    },
    iosContainer: {
        display: "flex",
        width: "100%",
        justifyContent: "center",
    },
    iosBox: {
        width: theme.spacing(30),
        height: theme.spacing(8),
        border: "1px solid #5b5b5b",
        borderRadius: theme.spacing(1),
        position: "absolute",
        transform: "translateY(-40px)",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.up('768')]: {
            transform: "translateY(-180px)",
            marginLeft: theme.spacing(-33)
        }
    },
    logo: {
        margin: theme.spacing(0, 2, 0, 2.6)
    },
    logoText: {
        color: theme.palette.common.white
    },
    playBox: {
        width: theme.spacing(30),
        height: theme.spacing(8),
        border: "1px solid #5b5b5b",
        borderRadius: theme.spacing(1),
        position: "absolute",
        transform: "translateY(45px)",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.up('768')]: {
            transform: "translateY(-180px)",
            marginLeft: theme.spacing(33)
        }
    },
    footer: {
        width: "100%",
        height: theme.spacing(1),
        backgroundColor: "#e85500",
        [theme.breakpoints.up('768')]: {
            height: theme.spacing(2)
        }
    },
    imgBox1Tablet: {
        width: "768px",
        margin: "auto",
        [theme.breakpoints.down('768')]: {
            display: "none"
        },
        [theme.breakpoints.up('1440')]: {
            display: "none"
        }

    },
    img1Tablet: {
        width: theme.spacing(79),
        objectFit: "contain",
        position: "absolute",
        transform: "translateY(-1120px)",
        [theme.breakpoints.down('768')]: {
            display: "none"
        },
        [theme.breakpoints.up('1440')]: {
            display: "none"
        }

    },
    imgBox1Desk: {
        width: "1300px",
        margin: "auto",
        [theme.breakpoints.down('1440')]: {
            display: "none"
        }
    },
    img1Desk: {
        width: theme.spacing(88.2),
        objectFit: "contain",
        position: "absolute",
        transform: "translateY(-1200px)",
        [theme.breakpoints.down('1440')]: {
            display: "none"
        }
    },
}))

const Footer = () => {
    const classes = useStyles();

    return (
        <Box>
            <Box className={classes.container}>
                <Box className={classes.imgBox}>
                    <img className={classes.img} src={iphone} alt="iphone"/>
                </Box>
                <Box className={classes.imgBox1}>
                    <img className={classes.img1} src={carpoolMadeEasy} alt="carpool"/>
                </Box>
                <Box className={classes.imgBox1Tablet}>
                    <img className={classes.img1Tablet} src={carpoolMadeEasyTablet} alt="carpool"/>
                </Box>
                <Box className={classes.imgBox1Desk}>
                    <img className={classes.img1Desk} src={carpoolMadeEasyDesk} alt="carpool"/>
                </Box>
                <Typography className={classes.title}> Join the band</Typography>
                <Typography className={classes.text}> a fresh way to share the road and make new
                    connections </Typography>
                <Box className={classes.iosContainer}>
                    <Box className={classes.iosBox}>
                        <img className={classes.logo} src={appleIcon} alt="logo"/>
                        <Typography className={classes.logoText}> Download for iOS</Typography>
                    </Box>
                    <Box className={classes.playBox}>
                        <img className={classes.logo} src={playIcon} alt="logo"/>
                        <Typography className={classes.logoText}> Download for Android</Typography>
                    </Box>
                </Box>
            </Box>
            <Box className={classes.footer}/>
        </Box>
    );
}

export default Footer;
