import React from "react";
import Drivers from "../../../photo/Driver.PNG";
import {Box, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme)=>({
    box:{
        width: theme.spacing(40),
        height: theme.spacing(67),
        margin: "auto",
        paddingTop: theme.spacing(8)

    }
}))

const Driver = () =>{
    const classes = useStyles();

    return(
        <Box className={classes.box}>
            <img src={Drivers} alt="drivers"/>
        </Box>
    );

}

export default Driver;
