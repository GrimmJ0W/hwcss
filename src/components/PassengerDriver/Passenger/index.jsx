import React from "react";
import {Typography, Box, makeStyles} from "@material-ui/core";
import "./style.css";
import img1 from "../../../photo/img-1@3x.png";

const useStyles = makeStyles((theme)=>({
    text:{
        fontFamily: "SFProDisplay-Semibold",
        fontSize: theme.spacing(2),
        paddingTop: theme.spacing(11),
        paddingBottom: theme.spacing(6),
        textAlign: "center"

    },
    box:{
        display: "flex",
        flexDirection: "column"

    },
    img:{
        width: theme.spacing(43),
        height: theme.spacing(13.5),
        margin: "auto",
        marginBottom: theme.spacing(36),
        [theme.breakpoints.up('767')]: {
            width: theme.spacing(84),
            height: theme.spacing(27),
            marginBottom: theme.spacing(10)
        }
    }
}))

const Passenger = () =>{
    const classes = useStyles();
    return(
        <Box className={classes.box}>
            <Typography className={classes.text}>
                See who's driving and ask to join
            </Typography>
            <img className={classes.img} src={img1} alt="img1"/>
        </Box>
    );
}

export default Passenger;
