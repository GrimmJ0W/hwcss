import React, {useContext, useState} from 'react';
import {Box, makeStyles, Typography} from "@material-ui/core";
import Logo from "../../photo/Logo.svg";
import AppleLogo from "../../photo/Apple logo.svg";
import playMarket from "../../photo/play market.svg";
import Scroll from "react-scroll";
import {AppContext} from "../../App";

const useStyles = makeStyles((theme) => ({
    mainNav:{
        position: "fixed",
        width: "100%",
        height: "auto",
        backgroundColor: theme.palette.common.white,
        zIndex: 2000,
    },
    navBar: {
        display: "flex",
        height: "50px",
        alignItems: "center",
        backgroundColor: theme.palette.common.white,
        justifyContent: "space-between",
        margin: theme.spacing(3),
        [theme.breakpoints.up('1440')]:{
            margin: theme.spacing(2, 17.5),
        }

    },
    iconsContainer: {
        display: "flex",
        flexDirection: "row-reverse",
        alignItems: "center",
        width: theme.spacing(24.875)

    },
    icons: {
        filter: "invert(100%)",
        cursor: "pointer"
    },
    iconsBox: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: theme.spacing(5),
        height: theme.spacing(5),
        marginLeft: theme.spacing(2),
        backgroundColor: theme.palette.common.white,
        "&:hover": {
            filter: "invert(100%)"
        }

    },
    text: {
        fontFamily: "AktivGrotesk-Medium",
        fontSize: theme.spacing(2),
        fontWeight: "bold",
        width: theme.spacing(12),
        [theme.breakpoints.down('768')]:{
            display: "none"
        }
    },
    navBarMenu:{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('1440')]:{
            display: "none"
        }
    },
    menu:{
        fontSize: theme.spacing(2),
        opacity: 0.5,
        margin: theme.spacing(0, 5),
        cursor: "pointer"
    },
    features:{
        fontSize: theme.spacing(2),
        opacity: 0.3,
        margin: theme.spacing(0, 5),
        cursor: "pointer"
    },
    selected:{
        fontSize: theme.spacing(2),
        margin: theme.spacing(0, 5),
        fontWeight: "bold",
        cursor: "pointer"

    }

}))

const scroll = Scroll.animateScroll;

const NavBar = () => {
    const {scrollToDrive, scrollToPassenger, setPassDrive} = useContext(AppContext)
    const [selected, setSelected] = useState(1);
    const classes = useStyles();

    return (
        <Box className={classes.mainNav}>
            <Box className={classes.navBar}>
                <Box>
                    <img src={Logo} alt="logo"/>
                </Box>
                <Box className={classes.navBarMenu}>
                    <Typography onClick={()=>setSelected(1)} className={(selected===1)? classes.selected : classes.menu} >Now</Typography>
                    <Typography onClick={()=>{
                        setSelected(2);
                        setPassDrive(1);
                        scrollToPassenger();
                    }} className={(selected===2)? classes.selected : classes.menu} >How to ride</Typography>
                    <Typography onClick={()=>{
                        setSelected(3);
                        setPassDrive(2);
                        scrollToDrive();
                    }} className={(selected===3)? classes.selected : classes.menu}>How to drive</Typography>
                    <Typography onClick={()=>setSelected(4)} className={(selected===4)? classes.selected : classes.features}>Features</Typography>
                </Box>
                <Box className={classes.iconsContainer}>
                    <Box className={classes.iconsBox} onClick={()=>scroll.scrollToBottom()} border={1} borderColor={'black'} borderRadius={8}>
                        <img className={classes.icons} src={AppleLogo} alt="apple logo"/>
                    </Box>
                    <Box className={classes.iconsBox} onClick={()=>scroll.scrollToBottom()} border={1} borderColor={'black'} borderRadius={8}>
                        <img className={classes.icons} src={playMarket} alt="play store logo"/>
                    </Box>
                    <Typography className={classes.text}>Get the app</Typography>
                </Box>
            </Box>
        </Box>
    );
}

export default NavBar;
