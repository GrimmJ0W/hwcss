import React from "react";
import {Box, makeStyles} from "@material-ui/core";
import the from "../../photo/the.svg"

const useStyles = makeStyles((theme) => ({
    phoneShadow: {
        height: theme.spacing(49),
        width: theme.spacing(27),
        margin: "130px auto 80px auto",
        backgroundColor: "#e9ecf0",
        borderRadius: theme.spacing(3),
        [theme.breakpoints.up('767')]:{
            height: theme.spacing(62),
            width: theme.spacing(34),
            margin: "200px auto 140px auto",
        }
    },
    img: {
        display: "flex",
        height: theme.spacing(12),
        objectFit: "fill",
        position: "absolute",
        zIndex: 1000,
        transform: "translateY(-330px)",
        [theme.breakpoints.up('767')]:{
            height: theme.spacing(21.5),
            transform: "translateY(-420px)",
        },
        [theme.breakpoints.up('1440')]:{
            height: theme.spacing(30),
            transform: "translateY(-520px)",
        }
    },
    imgContainer: {
        display: "flex",
        justifyContent: "center"
    }

}))

const FunWay = () => {
    const classes = useStyles();

    return (
        <Box>
            <Box className={classes.phoneShadow}/>
            <Box className={classes.imgContainer}>
                <img className={classes.img} src={the} alt="the"/>
            </Box>
        </Box>
    );
}
export default FunWay;
