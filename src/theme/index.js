import {createMuiTheme} from "@material-ui/core";
import WhyteInktrapRegular from "../Fonts/Whyteink/WhyteInktrap-Regular.ttf"

const theme = createMuiTheme({
    palette:{
        common:{
            black: "black",
            white: "white",
            backGround: "#f6f6f6"
        },
        switcher:{
            map: "#d2c65e",
            realTime: "#677993",
            public: "#677993"
        }
    },
    fontFamily:{
        WhyteInktrapRegular: WhyteInktrapRegular
    }
})

export default theme;
