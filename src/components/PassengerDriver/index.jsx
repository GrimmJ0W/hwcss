import React, {useContext, useState} from 'react';
import {Box, makeStyles} from '@material-ui/core';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import "./style.css";
import Passenger from "./Passenger";
import Driver from "./Driver";
import {AppContext} from "../../App";

const useStyles = makeStyles((theme) => ({
    btn: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: theme.spacing(37),
        height: theme.spacing(8),
        backgroundColor: theme.palette.common.backGround,
        borderRadius: theme.spacing(1),
        margin: theme.spacing(3)
    },
    btnBox: {
        display: "flex",
        width: theme.spacing(42),
        height: "auto",
        margin: "auto",
        [theme.breakpoints.up('767')]: {
            marginTop: theme.spacing(10),
            border: "1px solid #f6f6f6",
            borderRadius: theme.spacing(2),

        }
    },
    text: {
        fontSize: theme.spacing(2),
        fontFamily: "SFProDisplay-Semibold",
        fontWeight: "bold"
    },
    screen: {
        backgroundColor: theme.palette.common.backGround,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    container: {
        width: "100%",
        height: "auto",
        margin: theme.spacing(2),
        borderRadius: theme.spacing(2),
        backgroundColor: theme.palette.common.white,
        [theme.breakpoints.up('767')]: {
            margin: "40px auto",
            width: theme.spacing(88)
        },
        [theme.breakpoints.up('1440')]:{
            width: theme.spacing(142.7),
            margin: "80px auto"

        }

    }
}));

export default function SimpleBottomNavigation() {
    const classes = useStyles();
    const {passDrive, setPassDrive, driverRef, passengerRef} = useContext(AppContext)
    const [value, setValue] = useState(0);

    return (
        <Box className={classes.screen}>
            <Box className={classes.container}>
                <Box className={classes.btnBox}>
                    <Box className={classes.btn}>
                        <BottomNavigation
                            value={value}
                            onChange={(event, newValue) => {
                                setValue(newValue);
                            }}
                            showLabels
                        >
                            <BottomNavigationAction ref={passengerRef} onClick={() => setPassDrive(1)} className={classes.text}
                                                    label="Passenger"/>
                            <BottomNavigationAction ref={driverRef} onClick={() => setPassDrive(2)} className={classes.text}
                                                    label="Driver"/>
                        </BottomNavigation>
                    </Box>
                </Box>
                {passDrive === 1 &&
                <Passenger/>
                }
                {passDrive === 2 &&
                <Driver/>
                }
            </Box>
        </Box>
    );
}
