import React from "react";
import {Box, makeStyles, Typography} from "@material-ui/core";
import ReactPlayer from "react-player";
import video from "../../photo/video/bg-video.mp4";
import "./style.css"

const useStyles = makeStyles((theme) => ({
    video: {
        width: "100%",
        height: "480px",
        display: "flex",
        position: "relative",
        "& video":{
            objectFit: "cover",
        },
        [theme.breakpoints.up('767')]:{
            height:theme.spacing(101)

        }

    },
    videoHeight: {
        height: "480px",
        [theme.breakpoints.up('767')]:{
            height:theme.spacing(101)
        }
    },
    videoWidth: {
        [theme.breakpoints.up('767')]:{
            width: "768px"
        }
    },
    text: {
        fontSize: theme.spacing(4),
        fontFamily: "WhyteInktrap-Regular",
        textAlign: "left",
        margin: theme.spacing(0,3),
        color: theme.palette.common.white,
        paddingRight: theme.spacing(1),
        [theme.breakpoints.up('767')]:{
            fontSize: theme.spacing(9),
            transform: "translateY(-200px)"
        },
        [theme.breakpoints.up('1440')]:{
            transform: "translateY(-80px)",
        }
    },
    text2:{
        fontSize: theme.spacing(2),
        textAlign: "left",
        margin: theme.spacing(0,3),
        color: theme.palette.common.white,
        paddingTop: theme.spacing(2),
        [theme.breakpoints.up('767')]:{
            fontSize: theme.spacing(3),
            transform: "translateY(-200px)"
        },
        [theme.breakpoints.up('1440')]:{
            transform: "translateY(-80px)",
        }
    },
    textBox: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        position: "absolute",
        zIndex: 1000,
        transform: "translateY(-300px)",
    }


}))

const Video = () => {
    const classes = useStyles();

    return (
        <Box>
            <Box className={classes.video}>
                <ReactPlayer className={classes.test} id="video" width={classes.videoWidth} height={classes.videoHeight}  muted={true} playing={true} loop={true}
                             url={video}/>
            </Box>
            <Box className={classes.textBox} >
                <Typography className={classes.text}> now - is a fresh way to share the road and make new
                    connections.</Typography>
                <Typography className={classes.text2}> Choose to drive or ride with people already going your way</Typography>
            </Box>
        </Box>

    );
}

export default Video;
