import React from "react";
import container from "../../photo/containerMob@3x.png";
import containerDesk from "../../photo/container@3x.png"
import phone from "../../photo/mock@3x.png"
import {Box, makeStyles, Typography} from "@material-ui/core";
import "./style.css";

const useStyle = makeStyles((theme) => ({
    image: {
        width: "100%",
        height: theme.spacing(85),
        display: "flex",
        objectFit: "cover",
        [theme.breakpoints.up('767')]:{
            width: theme.spacing(88),
            margin: "auto"
        },
        [theme.breakpoints.up('1440')]:{
            display: "none"
        },
    },
    container: {
        marginTop: theme.spacing(7.5)
    },
    text: {
        position: "absolute",
        fontSize: theme.spacing(3),
        color: theme.palette.common.white,
        transform: "translateY(-670px)",
        zIndex: 1000,
        fontFamily: "SourceSansPro-Regular"
    },
    textContainer: {
        display: "flex",
        justifyContent: "center",
        margin: "auto",
        width: theme.spacing(35),
    },
    phone:{
        width: theme.spacing(35),
        position: "absolute",
        display: "flex",
        objectFit: "fill",
        zIndex: 1000,
        transform: "translateY(-500px)",
        [theme.breakpoints.up('767')]:{
            width: theme.spacing(38)

        }
    },
    imageDesk:{
        [theme.breakpoints.up('1440')]:{
            width: theme.spacing(173.25),
            height: theme.spacing(85),
            display: "flex",
            objectFit: "cover",
            margin: "auto"
        },
        [theme.breakpoints.down('1440')]:{
            display: "none"

        }

    }
}))

const AugmentedReality = () => {
    const classes = useStyle();

    return (
        <Box className={classes.container}>
            <img className={classes.image} src={container} alt="container"/>
            <img className={classes.imageDesk} src={containerDesk} alt="container"/>
            <Box className={classes.textContainer}>
                <Typography className={classes.text}>Augmented reality</Typography>
                <img className={classes.phone} src={phone} alt="phone" />
            </Box>
        </Box>
    );
}

export default AugmentedReality;
