import React from "react";
import {Box, makeStyles, Typography} from "@material-ui/core";
import mocku from "../../photo/mocku.png";
import "./style.css";

const useStyles = makeStyles((theme) => ({
    textContainer: {
        margin: theme.spacing(0, 3),
        textAlign: "center",
        paddingTop: theme.spacing(16),
        [theme.breakpoints.up('767')]: {
            margin: theme.spacing(0, 4),
            paddingTop: theme.spacing(30),
        },
        [theme.breakpoints.up('1440')]: {
            margin: theme.spacing(0, 27)

        }
    },
    text: {
        fontSize: theme.spacing(4),
        fontFamily: "WhyteInktrap-Regular",
        [theme.breakpoints.up('767')]: {
            fontSize: theme.spacing(8)
        },
        [theme.breakpoints.up('1440')]: {
            fontSize: theme.spacing(10),

        }

    },
    fun: {
        fontFamily: "Hanalei-Regular",
        transform: "rotate(-9deg)",
        display: "inline-block",
        [theme.breakpoints.up('767')]: {
            fontSize: theme.spacing(8)
        },
        [theme.breakpoints.up('1440')]: {
            fontSize: theme.spacing(10),
        }
    },
    photo: {
        display: "flex",
        justifyContent: "center",
        position: "relative",
        paddingTop: theme.spacing(1.46),
        objectFit: "fill",
        height: theme.spacing(46.75),
        [theme.breakpoints.up('767')]: {
            height: theme.spacing(77.5)
        }
    }
}));

const Intro = () => {
    const classes = useStyles();

    return (
        <Box>
            <Box className={classes.textContainer}>
                <Typography className={classes.text}>
                    carpooling made <b>easy, safe</b> and <span className={classes.fun}>fun</span> for <b>everyone</b>
                </Typography>
            </Box>
            <Box id="phone" className={classes.photo}>
                <img src={mocku} alt="mobile"/>
            </Box>
        </Box>
    );
}

export default Intro;
