import React, {createContext, useRef, useState} from 'react'
import NavBar from "./components/NavBar";
import './style.css';
import Intro from "./components/Intro";
import PassengerDriver from "./components/PassengerDriver";
import MapStyleSwitcher from "./components/mapStyleSwitcher";
import AugmentedReality from "./components/AugmentedReality";
import FunWay from "./components/FunWay";
import Video from "./components/Video";
import Footer from "./components/Footer";

export const AppContext = createContext({})

function App() {
  const driverRef = useRef("driver");
  const passengerRef = useRef("passenger");
  const [passDrive, setPassDrive] = useState(1)

  const scrollToDrive = () => driverRef.current.scrollIntoView();
  const scrollToPassenger = () => passengerRef.current.scrollIntoView();


  return (
      <AppContext.Provider value={{driverRef, passengerRef, passDrive, setPassDrive, scrollToPassenger, scrollToDrive}}>
        <NavBar/>
        <Intro/>
        <PassengerDriver/>
        <MapStyleSwitcher/>
        <AugmentedReality/>
        <FunWay/>
        <Video/>
        <Footer/>
      </AppContext.Provider>
  );
}

export default App;
